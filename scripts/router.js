'use strict';

angular.module('nessSmartApp').config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/login');
  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: '../views/main.html',
      controller: 'MainCtrl'
    })
    .state('login', {
      url: '/login',
      templateUrl: '../views/login.html',
      controller: 'loginCtrl'
    })
    .state('dashboard', {
      url: '/dashboard',
      templateUrl: '../views/dashboard.html',
      controller: 'dashCtrl'
    })
    .state('projects', {
      url: '/projects',
      templateUrl: '../views/projects.html',
      controller: 'projectCtrl'
    });
}]);
