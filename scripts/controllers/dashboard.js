(function (angular) {
  'use strict';

  angular.module('nessSmartApp').controller('dashCtrl', ['$scope', '$rootScope', '$timeout', '$compile', '$window', 'graphApi', 'dashboardServices', dashController]);

  function dashController($scope, $rootScope, $timeout, $compile, $window, graphApi, dashboardServices) {
    var angularDocument = angular.element(document);
    var sessionStorage = $window.sessionStorage;
    // var newDashboard;
    var dashObject = [{
      "name": "Company Dashboard",
      "loadOrder": [],
      "hiddenWidgets": []
    }];

    if (sessionStorage.getItem('dashboardStorage') === null) {
      sessionStorage.setItem('dashboardStorage', JSON.stringify(dashObject));
    }

    $scope.data1 = graphApi.linechart();

    function dashboards(index) {
      var loadDashboards = JSON.parse(sessionStorage.getItem('dashboardStorage'));
      $scope.currentDashboardIndex = index;

      $scope.dashboardData = loadDashboards;
      //get slide data from service.
      // function carouselSlides(id) {
      dashboardServices.slideDetails().then(function (result) {
        $scope.slideData = result;
        //timeout to ensure that carousel builds correctly.
        $timeout(function () {
          angularDocument.find('.owl-carousel').owlCarousel('destroy').owlCarousel({
            autoWidth: true,
            nav: true,
            dots: false,
            mouseDrag: false,
            navText: ['<i class="glyphicon glyphicon-menu-left""></i>', '<i class="glyphicon glyphicon-menu-right"></i>']
          });
        }, 500);
        angularDocument.find('.dashboard [data-dashboard="' + loadDashboards[index].name + '" ]').toggleClass('active');
        angularDocument.find('.dashboardNames p[data-dashboard="' + loadDashboards[index].name + '" ]').toggleClass('active');
        //build the widgets based off of the slide data.
        angular.forEach($scope.slideData, function (value, key) {
          var el = $compile('<div class="widget-wrapper"' + value.widgetDirective + ' data-widget="' + value.dataWidget + '"></div>')($scope);
          angularDocument.find('.dashboard [data-dashboard="' + loadDashboards[index].name + '" ]').append(el);
        });

        //load widget order and hide them.
        if (loadDashboards[index].hiddenWidgets) {
          angular.forEach(loadDashboards[index].hiddenWidgets, function (value, key) {
            angularDocument.find('.dashboard [data-dashboard="' + loadDashboards[index].name + '" ]' + ' div[data-widget="' + value + '" ]').addClass('widget-hidden');
            $timeout(function () {
              angularDocument.find('.slide[data-widget="' + value + '" ]').toggleClass('active');
            }, 500);
          });
        }
        if (loadDashboards[index].loadOrder) {
          angular.forEach(loadDashboards[index].loadOrder, function (value, key) {
            angularDocument.find('.dashboard [data-dashboard="' + loadDashboards[index].name + '" ]').append(angularDocument.find('.dashboard-wrapper div[data-widget="' + value + '" ]'));
          });
        }
        //to show number of widgets active.
        $timeout(function () {
          $scope.numOfWidgets = angularDocument.find('.slide.active').length;
        }, 500);

      });
      // }
    }
    dashboards(0);

    function releaseWidget() {
      dashboardServices.releaseStatus().then(function (result) {
        $scope.releaseData = result;
      });
    }
    releaseWidget();

    function pipelineTrendWidget() {
      dashboardServices.pipelineTrend().then(function (result) {
        $scope.pipelineTrendData = result;
      });
    }
    pipelineTrendWidget();

    function qualityTrendWidget() {
      dashboardServices.qualityTrend().then(function (result) {
        $scope.qualityTrendData = result;
      });
    }
    qualityTrendWidget();

    //save new Dashboards
    $scope.saveDashboard = function (name) {
      var newDashObject = [{
        "name": name,
        "loadOrder": [],
        "hiddenWidgets": ['releaseStatus', 'sprintTrend', 'pipelineTrend', 'effort', 'qualityTrend', 'featureBurndown']
      }];
      newDashObject = JSON.parse(sessionStorage.getItem('dashboardStorage')).concat(newDashObject);
      sessionStorage.setItem('dashboardStorage', JSON.stringify(newDashObject));
      dashboards(JSON.parse(sessionStorage.getItem('dashboardStorage')).length - 1);
      $scope.newDashboardName = '';
    };

    $scope.selectDashboard = function (index) {
      dashboards(index);
    };

    //show/hide widgets on clicking the carousel slide.
    angularDocument.on('click', '.slide', function (e) {
      angular.element(this).toggleClass('active');
      var widget = angular.element(this).data('widget');
      $scope.$apply(function () {
        $scope.numOfWidgets = angularDocument.find('.slide.active').length;
      });
      angularDocument.find('.dashboard-wrapper.active').prepend($('.dashboard-wrapper.active div[data-widget="' + widget + '" ]'));
      angularDocument.find('.dashboard-wrapper.active div[data-widget="' + widget + '" ]').toggleClass('widget-hidden');

    });

    //show/hide widgets when '-' is clicked
    angularDocument.on('click', '.widget-wrapper span.remove', function (e) {
      angular.element(this).parent().toggleClass('widget-hidden');
      var widget = angular.element(this).parent().data('widget');
      angularDocument.find('.slide[data-widget="' + widget + '" ]').toggleClass('active');
      $scope.$apply(function () {
        $scope.numOfWidgets = angularDocument.find('.slide.active').length;
      });
    });

    //Trigger drag and drop in edit mode and also save widgets order.
    var toggled = false;
    $scope.enableDrag = function () {
      var articleorder = [];
      var articleshidden = [];
      if (toggled) {
        angularDocument.find('.widget-wrapper').each(function (i) {
          if (!angular.element(this).hasClass("widget-hidden")) {
            articleorder.push(angular.element(this).attr('data-widget'));
          } else {
            articleshidden.push(angular.element(this).attr('data-widget'));
          }
        });

        var loadDashboards = JSON.parse(sessionStorage.getItem('dashboardStorage'));

        loadDashboards[$scope.currentDashboardIndex].loadOrder = articleorder;
        loadDashboards[$scope.currentDashboardIndex].hiddenWidgets = articleshidden;
        sessionStorage.setItem("dashboardStorage", JSON.stringify(loadDashboards));
        angularDocument.find('.sortable').sortable('destroy');
      } else {
        angularDocument.find('.sortable').sortable({
          connectWith: ".sortable"
        });
      }
      toggled = !toggled;
    };

    //revert changes when cancel button is clicked
    $scope.revertWidgets = function () {
      var loadDashboards = JSON.parse(sessionStorage.getItem('dashboardStorage'));
      toggled = !toggled;
      angularDocument.find('.sortable').sortable('destroy');
      angularDocument.find('.slide').addClass('active');
      angularDocument.find('.dashboard-wrapper div').removeClass('widget-hidden');
      if (loadDashboards[$scope.currentDashboardIndex].hiddenWidgets) {
        angular.forEach(loadDashboards[$scope.currentDashboardIndex].hiddenWidgets, function (value, key) {
          angularDocument.find('.dashboard [data-dashboard="' + loadDashboards[$scope.currentDashboardIndex].name + '" ]' + ' div[data-widget="' + value + '" ]').addClass('widget-hidden');
          angularDocument.find('.slide[data-widget="' + value + '" ]').toggleClass('active');
        });
      }
      if (loadDashboards[$scope.currentDashboardIndex].loadOrder) {
        angular.forEach(loadDashboards[$scope.currentDashboardIndex].loadOrder, function (value, key) {
          angularDocument.find('.dashboard [data-dashboard="' + loadDashboards[$scope.currentDashboardIndex].name + '" ]').append(angularDocument.find('.dashboard-wrapper div[data-widget="' + value + '" ]'));
        });
      }
    };

    // $scope.removeDashboard = function(e){
    //   var loadDashboards = JSON.parse(sessionStorage.getItem('dashboardStorage'));
    //   var dashboardToRemove = angular.element(e).prev();
    //   var index;
    //   console.log(dashboardToRemove);
    //   // for(var i = 0; i < loadDashboards.length; i += 1) {
    //   //   if(loadDashboards[i].name === dashboardToRemove) {
    //   //     index = i;
    //   //     break;
    //   //   }
    //   // }
    //   // console.log(index);
    //   // loadDashboards.splice(index, 1);
    //   // console.log(loadDashboards);
    //   // sessionStorage.setItem("dashboardStorage", JSON.stringify(loadDashboards));
    //   // dashboards(index-1);
    // };

    angularDocument.on('click', '.dashboardNames sup', function (e) {
      var loadDashboards = JSON.parse(sessionStorage.getItem('dashboardStorage'));
      var dashboardToRemove = angular.element(this).prev().data('dashboard');
      console.log(dashboardToRemove);
      var index;
      for(var i = 0; i < loadDashboards.length; i += 1) {
        if(loadDashboards[i].name === dashboardToRemove) {
          index = i;
          break;
        }
      }
      loadDashboards.splice(index, 1);
      sessionStorage.setItem("dashboardStorage", JSON.stringify(loadDashboards));
      dashboards(index-1);
    });

    
  }
})(angular);