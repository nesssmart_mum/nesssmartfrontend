'use strict';

/**
 * @ngdoc function
 * @name nessSmartApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nessSmartApp
 */
angular.module('nessSmartApp')
  .controller('MainCtrl', function (dashboardServices, $scope) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    function addonDetailsData() {
      dashboardServices.addonDetails().then(function (result) {
        $scope.addonData = result;
      });
    }
    addonDetailsData();
  });
