angular.module('nessSmartApp').factory('dashboardServices', ['$http', function ($http) {
  "use strict";

  var addonDetails = function () {
    return $http({
      url: 'http://jsonstub.com/dashboardAddonDetails',
      method: 'GET',
      dataType: 'json',
      data: '',
      headers: {
        'Content-Type': 'application/json',
        'JsonStub-User-Key': 'f3748baa-5483-4d92-bc8a-fb77f3c99872',
        'JsonStub-Project-Key': '7f69205d-b3c5-47f9-bad6-d603b579d27a'
      }
    }).then(function (response) {
      return response.data;
    });
  };

  var dashboardDetails = function () {
    return $http({
      url: 'http://jsonstub.com/dashboards',
      method: 'GET',
      dataType: 'json',
      data: '',
      headers: {
        'Content-Type': 'application/json',
        'JsonStub-User-Key': 'f3748baa-5483-4d92-bc8a-fb77f3c99872',
        'JsonStub-Project-Key': '7f69205d-b3c5-47f9-bad6-d603b579d27a'
      }
    }).then(function (response) {
      return response.data;
    });
  };

  var slideDetails = function () {
    return $http({
      url: 'http://jsonstub.com/slides',
      method: 'GET',
      dataType: 'json',
      data: '',
      headers: {
        'Content-Type': 'application/json',
        'JsonStub-User-Key': 'f3748baa-5483-4d92-bc8a-fb77f3c99872',
        'JsonStub-Project-Key': '7f69205d-b3c5-47f9-bad6-d603b579d27a'
      }
    }).then(function (response) {
      return response.data;
    });
  };

  var releaseStatus = function () {
    return $http({
      url: 'http://jsonstub.com/releaseStatus',
      method: 'GET',
      dataType: 'json',
      data: '',
      headers: {
        'Content-Type': 'application/json',
        'JsonStub-User-Key': 'f3748baa-5483-4d92-bc8a-fb77f3c99872',
        'JsonStub-Project-Key': '7f69205d-b3c5-47f9-bad6-d603b579d27a'
      }
    }).then(function (response) {
      return response.data;
    });
  };

  var pipelineTrend = function () {
    return $http({
      url: 'http://jsonstub.com/pipelineTrend',
      method: 'GET',
      dataType: 'json',
      data: '',
      headers: {
        'Content-Type': 'application/json',
        'JsonStub-User-Key': 'f3748baa-5483-4d92-bc8a-fb77f3c99872',
        'JsonStub-Project-Key': '7f69205d-b3c5-47f9-bad6-d603b579d27a'
      }
    }).then(function (response) {
      return response.data;
    });
  };

  var qualityTrend = function () {
    return $http({
      url: 'http://jsonstub.com/qualityTrend',
      method: 'GET',
      dataType: 'json',
      data: '',
      headers: {
        'Content-Type': 'application/json',
        'JsonStub-User-Key': 'f3748baa-5483-4d92-bc8a-fb77f3c99872',
        'JsonStub-Project-Key': '7f69205d-b3c5-47f9-bad6-d603b579d27a'
      }
    }).then(function (response) {
      return response.data;
    });
  };

  var saveWidgetOrder = function (data) {
    return $http({
      url: 'http://jsonstub.com/widgetOrder',
      method: 'PUT',
      dataType: 'json',
      data: '{articleOrder:' + data + '}',
      headers: {
        'Content-Type': 'application/json',
        'JsonStub-User-Key': 'f3748baa-5483-4d92-bc8a-fb77f3c99872',
        'JsonStub-Project-Key': '7f69205d-b3c5-47f9-bad6-d603b579d27a'
      }
    }).then(function (response) {
      return response.data;
    });
  };

  var loadWidgetOrder = function () {
    return $http({
      url: 'http://jsonstub.com/widgetOrder',
      method: 'GET',
      dataType: 'json',
      data: '',
      headers: {
        'Content-Type': 'application/json',
        'JsonStub-User-Key': 'f3748baa-5483-4d92-bc8a-fb77f3c99872',
        'JsonStub-Project-Key': '7f69205d-b3c5-47f9-bad6-d603b579d27a'
      }
    }).then(function (response) {
      return response.data;
    });
  };

  return {
    addonDetails: addonDetails,
    dashboardDetails: dashboardDetails,
    slideDetails: slideDetails,
    releaseStatus: releaseStatus,
    pipelineTrend: pipelineTrend,
    qualityTrend: qualityTrend,
    saveWidgetOrder: saveWidgetOrder,
    loadWidgetOrder: loadWidgetOrder
  };
}]);
