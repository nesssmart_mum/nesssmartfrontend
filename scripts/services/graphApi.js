angular.module('nessSmartApp').factory('graphApi', ['$http', function ($http) {
    "use strict";
    var graphApi = {};
    var lineJson = [
        { xdata: 1, ydata: 54 },
        { xdata: 2, ydata: 66 },
        { xdata: 3, ydata: 77 },
        { xdata: 4, ydata: 70 },
        { xdata: 5, ydata: 60 },
        { xdata: 6, ydata: 63 },
        { xdata: 7, ydata: 55 },
        { xdata: 8, ydata: 47 },
        { xdata: 9, ydata: 55 },
        { xdata: 10, ydata: 30 }
    ];

    graphApi.linechart = function () {
        return lineJson;
    };

    return graphApi;
}]);