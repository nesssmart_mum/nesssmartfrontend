(function (angular) {
  'use strict';
/*
  -> The custom directive helps in creating a linechart, will read the data from the attribute chart-data.
  -> The link function will contain the code to construct the graph.
*/
  angular.module('nessSmartApp').directive('lineChart', function ($window) {
    return {
      restrict: 'EA',
      template: "<svg height ='280px' width ='540px'></svg>",
      link: function (scope, elem, attrs) {
        var ydataDataToPlot = scope[attrs.chartData];
        var padding = 20;
        var pathClass = "path";
        var xScale, yScale, xAxisGen, yAxisGen, linegraph, area;
        var d3 = $window.d3;
        var rawSvg = elem.find("svg")[0];
        var svg = d3.select(rawSvg);
        function setChartParameters() {
          xScale = d3.scale.linear()
            .domain([ydataDataToPlot[0].xdata, ydataDataToPlot[ydataDataToPlot.length - 1].xdata])
            .range([padding + 5, rawSvg.clientWidth - padding]);

          yScale = d3.scale.linear()
            .domain([0, d3.max(ydataDataToPlot, function (d) {
              return d.ydata;
            })])
            .range([rawSvg.clientHeight - padding, padding]);

          xAxisGen = d3.svg.axis()
            .scale(xScale)
            .orient("bottom")
            .ticks(ydataDataToPlot.length - 1);

          yAxisGen = d3.svg.axis()
            .scale(yScale)
            .orient("left")
            .ticks(5);

          // area = d3.svg.area()
          //             .x(function(d){ return xScale(d.xdata); })
          //             .y0(280)
          //             .y1(function (d) {
          //     return yScale(d.ydata);
          //   });

          linegraph = d3.svg.line()
            .x(function (d) {
              return xScale(d.xdata);
            })
            .y(function (d) {
              return yScale(d.ydata);
            })
            .interpolate("linear");
        }

        function drawLineChart() {

          setChartParameters();

          svg.append("svg:g")
            .attr("class", "x axis")
            .attr("transform", "translate(0,260)")
            .call(xAxisGen);

          svg.append("svg:g")
            .attr("class", "y axis")
            .attr("transform", "translate(20,0)")
            .call(yAxisGen);

          svg.append("svg:path")
            .attr({
              d: linegraph(ydataDataToPlot),
              "stroke": "#0c6399",
              "stroke-width": 3,
              "fill": "#f2f7fa",
              "class": pathClass
            });

          svg.append("svg:path")
            .attr("class", "area")
            .attr("d", area);

          svg.selectAll("dot")
            .data(ydataDataToPlot)
            .enter().append("circle")
            .attr('class', 'line-dot')
            .attr("r", 6)
            .attr("cx", function (d) { return xScale(d.xdata); })
            .attr("cy", function (d) { return yScale(d.ydata); });
        }

        drawLineChart();
      }
    };
  });

})(angular);