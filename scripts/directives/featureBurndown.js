(function (angular) {
  'use strict';
  angular.module('nessSmartApp').directive('featureBurndown', ['$window', 'dashboardServices', function ($window, dashboardServices) {
    return {
      restrict: 'A',
      templateUrl: '../../views/widgets/featureBurndown.html'
    };
  }]);

})(angular);