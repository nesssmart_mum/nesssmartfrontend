(function (angular) {
  'use strict';
  angular.module('nessSmartApp').directive('pipelineTrend', ['$window', 'dashboardServices', function ($window, dashboardServices) {
    return {
      restrict: 'A',
      templateUrl: '../../views/widgets/pipelineTrend.html'
    };
  }]);

})(angular);