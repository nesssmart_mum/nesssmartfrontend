(function (angular) {
  'use strict';
  angular.module('nessSmartApp').directive('qualityTrend', ['$window', 'dashboardServices', function ($window, dashboardServices) {
    return {
      restrict: 'A',
      templateUrl: '../../views/widgets/qualityTrend.html'
    };
  }]);

})(angular);