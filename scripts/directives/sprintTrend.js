(function (angular) {
  'use strict';
  angular.module('nessSmartApp').directive('sprintTrend', ['$window', 'dashboardServices', function ($window, dashboardServices) {
    return {
      restrict: 'A',
      templateUrl: '../../views/widgets/sprintTrend.html'
    };
  }]);

})(angular);