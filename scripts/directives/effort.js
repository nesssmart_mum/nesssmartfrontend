(function (angular) {
  'use strict';
  angular.module('nessSmartApp').directive('effort', ['$window', 'dashboardServices', function ($window, dashboardServices) {
    return {
      restrict: 'A',
      templateUrl: '../../views/widgets/effort.html'
    };
  }]);

})(angular);
