(function (angular) {
  'use strict';
  angular.module('nessSmartApp').directive('releaseStatus', ['$window', 'dashboardServices', function ($window, dashboardServices) {
    return {
      restrict: 'A',
      templateUrl: '../../views/widgets/releaseStatus.html'
    };
  }]);

})(angular);
