'use strict';

/**
 * @ngdoc overview
 * @name nessSmartApp
 * @description
 * # nessSmartApp
 *
 * Main module of the application.
 */
angular.module('nessSmartApp', ['ui.router', 'ngAnimate']);
