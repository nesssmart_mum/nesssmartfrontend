class AssessmentService {
    constructor(constants, apiService) {
        'ngInject'
        this.baseUrl = constants.apiUrl + 'assessment/';
        this.apiService = apiService;
    }

    getAssessmentList() {
        var url = this.baseUrl + 'getAssessmentTitles';
        return this.apiService.getData(url);
    };

    getAssessment(assessmentId) {
        var url = this.baseUrl + 'getAssessmentById?assessmentId=' + assessmentId;
        return this.apiService.getData(url);
    };

    getFeedbackList() {
        var url = this.baseUrl + 'getAssessmentResponseMapping';
        return this.apiService.getData(url);
    };

    getFeedback(responseId) {
        var url = this.baseUrl + 'getAssessmentResponseById?assessmentResponseId=' + responseId;
        return this.apiService.getData(url);
    };

    getFeedbackReport(responseId) {
        var url = this.baseUrl + 'getAssessmentReportById?assessmentResponseId=' + responseId;
        return this.apiService.getData(url);
    };

    deleteFeedback(responseId) {
        var url = this.baseUrl + 'deleteAssessmentResponse?assessmentResponseId=' + responseId;
        return this.apiService.deleteData(url);
    };

    saveFeedback(data) {
        var url = this.baseUrl + 'saveAssessmentResponse';
        return this.apiService.postData(url, data);
    };
}

export default AssessmentService;