import ConfirmDeleteController from '../components/popups/confirmDelete.controller';
class ManageFeedbackController {
    constructor($scope, $state, $uibModal, assessmentService) {
        'ngInject'

        this.getFeedback = function() {
            assessmentService.getFeedbackList().then(
                (response) => {
                    this.feedbackList = response.data;
                }
            );
        };

        this.viewReport = function(responseId) {
            $state.go('assessment.report', { responseId: responseId });
        };

        this.editFeedback = function(responseId) {
            $state.go('assessment.editFeedback', { feedbackId: responseId });
        };

        this.deleteFeedback = function(feedback) {
            $scope.message = 'Are you sure you want to delete this feedback ?';
            $uibModal.open({
                templateUrl: 'components/popups/confirmDelete.html',
                controller: ConfirmDeleteController,
                controllerAs: 'ctrl',
                size: 'md',
                scope: $scope
            }).result.then(() => {
                assessmentService.deleteFeedback(feedback.id).then(() => {
                    var aIndex = this.feedbackList.indexOf(feedback);
                    this.feedbackList.splice(aIndex, 1);
                });
            });
        };
    }
}

export default ManageFeedbackController;