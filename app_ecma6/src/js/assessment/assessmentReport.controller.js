import ViewFeedbackController from './viewFeedback.controller';
class AssessmentReportController {
    constructor($scope, $stateParams, $uibModal, assessmentService) {
        'ngInject'

        this.getFeedback = function() {
            assessmentService.getFeedbackReport($stateParams.responseId).then(
                (response) => {
                    this.feedbackReport = response.data;
                }
            );
            assessmentService.getFeedback($stateParams.responseId).then(
                (response) => {
                    this.assessmentFeedback = response.data;
                }
            );
        };

        this.getProgressType = function(rating) {
            var type = ''
            if (rating > 70) {
                type = 'success';
            } else if (rating > 30) {
                type = 'warning';
            } else {
                type = 'danger';
            }
            return type;
        };

        this.viewResponse = function() {
            $scope.assessmentFeedback = this.assessmentFeedback;
            $uibModal.open({
                templateUrl: 'assessment/viewFeedback.html',
                controller: ViewFeedbackController,
                controllerAs: 'ctrl',
                size: 'lg',
                scope: $scope
            });
        };
    }
}

export default AssessmentReportController;