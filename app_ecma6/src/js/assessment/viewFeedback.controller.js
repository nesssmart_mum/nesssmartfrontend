class ViewFeedbackController {
    constructor($scope, $window, $uibModalInstance) {
        'ngInject'

        let currentIndex = 0;
        this.assessment = $scope.assessmentFeedback;
        this.sectionList = this.assessment.sectionList;
        this.section = this.sectionList[currentIndex];

        this.showPrev = function() {
            return currentIndex > 0;
        };

        this.showNext = function() {
            return currentIndex < this.sectionList.length - 1;
        };

        this.goToPrev = function() {
            currentIndex--;
            this.section = this.sectionList[currentIndex];
            //$window.scrollTo(0, 0);
            $('.modal').animate({scrollTop: 0}, "fast");
        };

        this.goToNext = function() {
            currentIndex++;
            this.section = this.sectionList[currentIndex];
            //$window.scrollTo(0, 0);
            $('.modal').animate({scrollTop: 0}, "fast");
        };

        this.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

    }
}

export default ViewFeedbackController;