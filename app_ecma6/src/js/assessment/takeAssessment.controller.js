import InfoController from '../components/popups/info.controller';
class TakeAssessmentController {
    constructor($scope, $rootScope, $state, $stateParams, $uibModal, $window, assessmentService, utilService) {
        'ngInject'

        let currentIndex = 0;
        this.showAssessment = false;

        this.getAssessments = function() {
            if ($stateParams.feedbackId) {
                assessmentService.getFeedback($stateParams.feedbackId).then(
                    (response) => {
                        this.assessment = response.data;
                        this.sectionList = this.assessment.sectionList;
                        this.section = this.sectionList[currentIndex];
                        this.showAssessment = true;
                    }
                );
            } else {
                assessmentService.getAssessmentList().then(
                    (response) => {
                        this.assessmentList = response.data;
                    }
                );
            }
        };

        this.getAssessment = function() {
            if (!this.assessmentId) {
                this.showAssessment = false;
            }
            assessmentService.getAssessment(this.assessmentId).then(
                (response) => {
                    this.assessment = response.data;
                    this.sectionList = this.assessment.sectionList;
                    this.section = this.sectionList[currentIndex];
                    this.showAssessment = true;
                }
            );
        };

        this.hideAssessment = function() {
            if ($stateParams.feedbackId) {
                $state.go('assessment.manageFeedback');
            } else {
                this.showAssessment = false;
                this.assessmentId = '';
                currentIndex = 0;
            }

        };

        this.selectOption = function(optionList, option) {
            angular.forEach(optionList, function(option) {
                option.selected = 'false';
            });
            option.selected = 'true';
        };

        this.showPrev = function() {
            return currentIndex > 0;
        };

        this.showNext = function() {
            return currentIndex < this.sectionList.length - 1;
        };

        this.goToPrev = function() {
            currentIndex--;
            this.section = this.sectionList[currentIndex];
            $window.scrollTo(0, 0);
        };

        this.goToNext = function() {
            currentIndex++;
            this.section = this.sectionList[currentIndex];
            $window.scrollTo(0, 0);
        };

        this.submitFeedback = function() {
            this.assessment.userId = $rootScope.currentUser.userId;
            this.assessment.totalScore = 0;
            this.initializeLevelScoring();
            assessmentService.saveFeedback(this.assessment).then((response) => {
                $scope.message = 'Feedback saved successfully';
                $uibModal.open({
                    templateUrl: 'components/popups/info.html',
                    controller: InfoController,
                    controllerAs: 'ctrl',
                    size: 'sm',
                    scope: $scope
                }).result.finally(() => {
                    $state.go('assessment.report', { responseId: response.data.assessmentResponseId });
                });
            });
        };
    }

    initializeLevelScoring() {
        angular.forEach(this.assessment.sectionList, function(section) {
            angular.forEach(section.levelList, function(level) {
                level.completion = 0;
                level.rating = 0;
            });
        });
    };
}

export default TakeAssessmentController;