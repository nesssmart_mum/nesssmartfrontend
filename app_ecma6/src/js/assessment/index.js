import angular from 'angular';
import TakeAssessmentController from './takeAssessment.controller';
import AssessmentReportController from './assessmentReport.controller';
import ManageFeedbackController from './manageFeedback.controller';
import ViewFeedbackController from './viewFeedback.controller';
import AssessmentService from './assessment.service';

let assessmentModule = angular.module('nessSmart.assessment', []);

assessmentModule.controller('takeAssessmentController', TakeAssessmentController);
assessmentModule.controller('assessmentReportController', AssessmentReportController);
assessmentModule.controller('manageFeedbackController', ManageFeedbackController);
assessmentModule.controller('viewFeedbackController', ViewFeedbackController);
assessmentModule.service('assessmentService', AssessmentService);

export default assessmentModule;