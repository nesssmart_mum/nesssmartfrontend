import $ from 'jquery';
import 'jqueryui';
import 'bootstrap';
import angular from 'angular';
import 'angular-ui-router';
import 'angular-ui-bootstrap';
import 'angular-ui-sortable';
import constants from './config/app.constants';
import appConfig from './config/app.config';
import appRun from './config/app.run';
import './config/app.templates';
import './login';
import './components';
import './home';
import './admin';
import './assessment';
import './common';
import 'c3-angular';

const requires = [
    'ui.router',
    'ui.bootstrap',
    'templates',
    'dashboard',
    'nessSmart.login',
    'nessSmart.home',
    'nessSmart.admin',
    'nessSmart.assessment',
    'nessSmart.common',
    'gridshore.c3js.chart',
];

let nessSmartApp = angular.module('nessSmart', requires);

nessSmartApp.constant('constants', constants);

nessSmartApp.config(appConfig);

nessSmartApp.run(appRun);

angular.element(document).ready(() => {
    angular.bootstrap(document, ['nessSmart'], {
        strictDi: true
    });
});