class ConfimrDeleteController {
    constructor($scope, $uibModalInstance) {
        'ngInject'

        this.message = $scope.message;

        this.no = function() {
            $uibModalInstance.dismiss('cancel');
        }

        this.yes = function() {
            $uibModalInstance.close();
        }
    }
}

export default ConfimrDeleteController;