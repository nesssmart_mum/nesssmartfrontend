class InfoController {
    constructor($scope, $uibModalInstance) {
        'ngInject'

        this.message = $scope.message;

        this.ok = function() {
            $uibModalInstance.close(false);
        }
    }
}

export default InfoController;