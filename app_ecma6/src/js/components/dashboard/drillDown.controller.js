class DrillDownController {
    constructor($scope, $filter, $uibModalInstance) {
        'ngInject'

        this.title = $scope.title || $scope.data.name;
        this.drillDownHeader = [];

        if ($scope.data.widgetData) {
            angular.forEach($scope.data.widgetData, (data) => {
                var level = $scope.level.toLowerCase();
                if (level && data[level]) {
                    this.showLevel = true;
                    angular.forEach(data[level], (level) => {
                        this.drillDownHeader.push({
                            name: level.name,
                            value: level.value,
                            arrowValue: level.arrowValue,
                            unitValue: level.unitValue,
                            isMultiple: true
                        });
                    })
                } else if (level && level == data.name.toLowerCase()) {
                    this.showLevel = false;
                    this.drillDownHeader.push({
                        name: data.name,
                        value: data.value,
                        arrowValue: data.arrowValue,
                        unitValue: data.unitValue,
                        isMultiple: true
                    });
                } else if (!level) {
                    this.drillDownHeader.push({
                        name: data.name,
                        value: data.value,
                        arrowValue: data.arrowValue,
                        unitValue: data.unitValue,
                        isMultiple: true
                    });
                }
            });
        } else {
            this.drillDownHeader.push({
                name: $scope.data.name,
                value: $scope.data.value,
                arrowValue: $scope.data.arrowValue,
                unitValue: $scope.data.unitValue
            });
        }
        this.drillDown = angular.copy($scope.data.drillDown);

        if ($scope.type === 'chart') {
            this.plotChart = function () {
                this.names = [];
                this.dataPoints = [];
                this.dataColumns = [];
                var i = 0;
                angular.forEach(this.drillDown.drillDownData, (data) => {
                    this.dataPoints[i] = {
                        x: data.name
                    };
                    angular.forEach(data.sections, (section) => {

                        this.dataPoints[i][section.title] = section.value;

                        this.names.push(section.title);

                        this.dataColumns.push({
                            "id": section.title,
                            "type": "spline"
                        });
                    });

                    this.datax = {
                        "id": "x"
                    };
                    i++;
                });
            };

            this.handleCallback = (chartObj) => {
                this.theChart = chartObj;
            };

            if ($scope.level) {
                var filteredObj = angular.copy($scope.data.drillDown.drillDownData);
                angular.forEach(filteredObj, (obj) => {
                    obj.sections = obj.sections.filter((section) => {
                        debugger;
                        if (section.title.split('_')[1] == $scope.level) {
                            section.title = section.title.split('_')[0];
                            return true;
                        } else if (section.title.toLowerCase() == $scope.level.toLowerCase()) {
                            return true;
                        } else {
                            return false;
                        }
                    })
                });
                this.drillDown.drillDownData = filteredObj;
                this.drillDown.yLabel = $scope.yLabel || this.drillDown.yLabel;
            }

            this.plotChart();
        } else if ($scope.type === 'table') {
            angular.forEach(this.drillDown.drillDownData, (data) => {
                angular.forEach(data.versionData, (version) => {
                    data[version.environment] = version.week + 'w';
                });
            });
        } else {
            this.showPercent = true;
            this.selectedMonth = this.drillDown.drillDownData ? this.drillDown.drillDownData[0].month : new Date().toLocaleDateString('en-us', {
                month: 'short'
            });
            this.deploymentList = this.drillDown.drillDownData ? this.drillDown.drillDownData[0].buildVersion : "";

            this.updateDeploymentList = function () {
                this.deploymentList = $filter('filter')(this.drillDown.drillDownData, {
                    month: this.selectedMonth
                })[0].buildVersion;
            };
        }

        this.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
}

export default DrillDownController;