import DrillDownController from './drillDown.controller';
var WidgetComponent = {
    bindings: {
        dashboardConfig: '=',
        widgetData: '=',
        widgetInfo: '=',
        edit: '=',
        removeWidget: '&'
    },
    controller: function($scope, $uibModal, componentsConstants) {
        'ngInject'

        this.getTemplateUrl = function() {
            return this.widgetInfo.templateUrl;
        };

        this.showDrillDown = function(type, data, title, level, yLabel) {
            var template = '';
            if (type === 'chart') {
                template = componentsConstants.drillDownChartTemplateUrl
            } else if (type === 'table') {
                template = componentsConstants.drillDownTableTemplateUrl
            } else {
                template = componentsConstants.drillDownListTemplateUrl
            }

            $scope.data = data;
            $scope.type = type;
            $scope.title = title;
            $scope.level = level || "";
            $scope.yLabel = yLabel || "";

            $uibModal.open({
                templateUrl: template,
                controller: DrillDownController,
                controllerAs: 'ctrl',
                size: 'md',
                scope: $scope
            });
        };
    },
    controllerAs: 'ctrl',
    template: '<div class="widget-wrapper {{ctrl.dashboardConfig.theme}}-widget" ng-class="{draggable: ctrl.edit}">' +
        '<span class="widget-buttons"><i class="fa fa-play" aria-hidden="true" title="play"></i> &nbsp; <i class="fa fa-pause" aria-hidden="true" title="pause"></i></span>' +
        '<div ng-include="ctrl.getTemplateUrl()"></div>' +
        '<span class="remove" ng-show="ctrl.edit" ng-click="ctrl.removeWidget({widget: ctrl.widgetInfo})">&minus;</span>' +
        '</div>'
};

export default WidgetComponent;