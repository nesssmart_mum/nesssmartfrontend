import DashboardConfigurationController from './dashboardConfiguration.controller';
import ConfirmDeleteController from '../popups/confirmDelete.controller';
import InfoController from '../popups/info.controller';

class DashboardController {
    constructor($scope, $rootScope, $window, $filter, $uibModal, componentsConstants, dashboardService) {
        'ngInject'

        let userId = ($rootScope.currentUser) ? $rootScope.currentUser.userId : '';
        let tempTitle = '';
        let tempWidgets = [];
        this.currentIndex = 0;
        this.edit = false;
        this.newDashboard = false;
        this.sortOptions = {
            disabled: true
        };
        this.noWrapSlides = false;
        this.active = 0;

        this.initializeDashboard = function(dashboardData) {
            this.dashboardData = dashboardData;
            this.dashboardConfiguration = this.dashboardData.dashboardConfiguration;
            this.dashboardList = this.dashboardData.dashboardList;
            this.currentDashboard = this.dashboardList[0];
        };

        if (componentsConstants.useApi) {
            dashboardService.getDashboardData(userId).then((response) => {
                this.initializeDashboard(response.data);
            });
        } else {
            //------------- Will not be required when db is reaady--------------------//
            if (!$window.sessionStorage['dashboardData']) {
                let dashObject = {
                    title: 'Company Dashboard',
                    userId: 'user1',
                    widgetList: []
                }
                angular.forEach(this.widgets, (widget) => {
                    dashObject.widgets.push(widget);
                });
                let dashData = {
                    userId: 'user1',
                    dashboardConfiguration: {
                        columnCount: '2',
                        theme: 'dark'
                    },
                    dashboardList: [dashObject]
                }
                $window.sessionStorage['dashboardData'] = angular.toJson(dashData);
            }
            this.initializeDashboard(JSON.parse($window.sessionStorage['dashboardData']));
            //-------------------------------------------------------------------------//
        }

        this.toggleWidget = function(widget) {
            if (this.hasWidget(widget)) {
                this.removeWidget(widget);
            } else {
                this.addWidget(widget);
            }
        };

        this.addWidget = function(widget) {
            this.currentDashboard.widgetList.push(widget);
            this.numOfWidgets = this.currentDashboard.widgetList.length;
        };

        this.removeWidget = function(widget) {
            var arrWidgets = $filter('filter')(this.currentDashboard.widgetList, { index: widget.index });
            var wIndex = this.currentDashboard.widgetList.indexOf(arrWidgets[0]);
            this.currentDashboard.widgetList.splice(wIndex, 1);
            this.numOfWidgets = this.currentDashboard.widgetList.length;
        };

        this.hasWidget = function(widget) {
            if (this.currentDashboard) {
                var arrWidgets = $filter('filter')(this.currentDashboard.widgetList, { index: widget.index });
                return arrWidgets.length > 0;
            }
            return false;
        };

        this.showEditor = function() {
            tempTitle = this.currentDashboard.title;
            tempWidgets = angular.copy(this.currentDashboard.widgetList);
            this.numOfWidgets = this.currentDashboard.widgetList.length;
            //------- May be changed in future ------------------//
            //this.slideData = this.widgets;
            this.slideData = [];
            let slideGroup = [];
            let slideIndex = 0;
            for (let i = 0; i < this.widgets.length; i++) {
                slideGroup.push(this.widgets[i]);
                if ((i + 1) % 3 == 0 || i == this.widgets.length - 1) {
                    this.slideData.push({ index: slideIndex, slides: slideGroup });
                    slideIndex++;
                    slideGroup = [];
                }
            }
            //--------------------------------------//
            this.sortOptions.disabled = false;
            this.edit = true;
        };

        this.showConfiguration = function() {
            $scope.configuration = this.dashboardConfiguration;
            $uibModal.open({
                templateUrl: componentsConstants.configurationTemplateUrl,
                controller: DashboardConfigurationController,
                controllerAs: 'ctrl',
                size: 'md',
                scope: $scope
            }).result.then((config) => {
                this.dashboardConfiguration = config;
                if (componentsConstants.useApi) {
                    $rootScope.currentUser.dashboardConfig = config;
                    dashboardService.editConfiguration($rootScope.currentUser);
                } else {
                    this.dashboardData.dashboardConfiguration = config;
                    $window.sessionStorage['dashboardData'] = JSON.stringify(this.dashboardData);
                }
            });
        };

        this.showFullScreen = function() {
            var i = document.documentElement;

            if (i.requestFullscreen) {
                i.requestFullscreen();
            } else if (i.webkitRequestFullscreen) {
                i.webkitRequestFullscreen();
            } else if (i.mozRequestFullScreen) {
                i.mozRequestFullScreen();
            } else if (i.msRequestFullscreen) {
                i.msRequestFullscreen();
            }
            angular.element('nav').hide();
            angular.element('#dashboardHeader').hide();
        };

        $(document).on('webkitfullscreenchange mozfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
            let isFullScreen = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
            if (!isFullScreen) {
                angular.element('nav').show();
                angular.element('#dashboardHeader').show();
            }
        });

        this.showAsessmentSaved = function() {
            $scope.message = 'Dashboard saved successfully';
            $uibModal.open({
                templateUrl: componentsConstants.infoTemplateUrl,
                controller: InfoController,
                controllerAs: 'ctrl',
                size: 'sm',
                scope: $scope
            });
        };

        this.saveDashboard = function() {
            if (!this.currentDashboard.title) {
                this.errorText = 'Dashboard Title is mandatory';
                this.showError = true;
                return;
            }
            if (this.newDashboard) {
                this.dashboardList.push(this.currentDashboard);
                this.currentIndex = this.dashboardList.length - 1;
                this.newDashboard = false;
            }
            this.edit = false;
            this.sortOptions.disabled = true;
            if (componentsConstants.useApi) {
                dashboardService.saveDashboard(this.currentDashboard).then(() => {
                    this.showAsessmentSaved();
                });
            } else {
                $window.sessionStorage['dashboardData'] = JSON.stringify(this.dashboardData);
                this.showAsessmentSaved();
            }
        };

        this.manageDeletion = function() {
            if (this.currentIndex == 0) {
                this.dashboardList.splice(0, 1);
                this.currentDashboard = this.dashboardList[0];
            } else {
                this.currentDashboard = this.dashboardList[this.currentIndex - 1];
                this.dashboardList.splice(this.currentIndex, 1);
                this.currentIndex = this.currentIndex - 1;
            }
            this.edit = false;
            this.sortOptions.disabled = true;
        };

        this.deleteDashboard = function() {
            $scope.message = 'Are you sure you want to delete dashboard?';
            $uibModal.open({
                templateUrl: componentsConstants.confirmDeleteTemplateUrl,
                controller: ConfirmDeleteController,
                controllerAs: 'ctrl',
                size: 'md',
                scope: $scope
            }).result.then(() => {
                if (componentsConstants.useApi) {
                    dashboardService.deleteDashboard(this.currentDashboard.dashboardId).then(() => {
                        this.manageDeletion();
                    });
                } else {
                    this.manageDeletion();
                    $window.sessionStorage['dashboardData'] = JSON.stringify(this.dashboardData);
                }
            });
        };

        this.revertChanges = function() {
            this.sortOptions.disabled = true;
            this.edit = false;
            if (this.newDashboard) {
                this.currentDashboard = this.dashboardList[this.currentIndex];
                this.newDashboard = false;
            } else {
                this.currentDashboard.title = tempTitle;
                this.currentDashboard.widgetList = tempWidgets;
            }
        };

        this.selectDashboard = function(index) {
            this.currentIndex = index;
            this.currentDashboard = this.dashboardList[index];
        };

        this.createDashboard = function() {
            this.currentDashboard = {
                title: '',
                userId: userId,
                widgetList: []
            };
            this.newDashboard = true;
            this.showEditor();
        };
    }
}

export default DashboardController;