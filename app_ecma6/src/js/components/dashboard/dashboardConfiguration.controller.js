class DashboardConfigurationController {
    constructor($scope, $uibModalInstance) {
        'ngInject'

        this.configuration = angular.copy($scope.configuration);

        this.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        }

        this.saveConfiguration = function() {
            $uibModalInstance.close(this.configuration);
        }
    }
}

export default DashboardConfigurationController;