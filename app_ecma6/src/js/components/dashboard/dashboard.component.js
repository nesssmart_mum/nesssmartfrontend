import DashboardController from './dashboard.controller';
import ComponentsConstants from '../components.constants';

var DashboardComponent = {
    templateUrl: ComponentsConstants.dashboardTemplateUrl,
    bindings: {
        widgets: '=',
        widgetsData: '='
    },
    controller: DashboardController,
    controllerAs: 'ctrl'
};

export default DashboardComponent;