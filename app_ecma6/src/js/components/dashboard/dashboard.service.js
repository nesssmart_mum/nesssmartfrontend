class DashboardService {
    constructor(componentsConstants, $http) {
        'ngInject'
        this.dashboardConstants = componentsConstants;
        this.http = $http;
    }

    getDashboardData(userId) {
        var apiUrl = this.dashboardConstants.getDashboardDataApi + userId;
        return this.http({
            url: apiUrl,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });
    };

    deleteDashboard(dashboardId) {
        var apiUrl = this.dashboardConstants.deleteDashboardApi + dashboardId;
        return this.http({
            url: apiUrl,
            method: 'DELETE'
        });
    };

    saveDashboard(data) {
        var apiUrl = this.dashboardConstants.saveDashboardApi;
        return this.http({
            url: apiUrl,
            method: 'POST',
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    };

    editConfiguration(data) {
        var apiUrl = this.dashboardConstants.editConfigurationApi;
        return this.http({
            url: apiUrl,
            method: 'POST',
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    };
}

export default DashboardService;