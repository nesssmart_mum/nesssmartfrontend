import angular from 'angular';
import DashboardController from './dashboard/dashboard.controller';
import DashboardConfigurationController from './dashboard/dashboardConfiguration.controller';
import DrillDownController from './dashboard/drillDown.controller';
import DashboardService from './dashboard/dashboard.service';
import WidgetComponent from './dashboard/widget.component';
import DashboardComponent from './dashboard/dashboard.component';
import ComponentsConstants from './components.constants';
import ConfirmDeleteController from './popups/confirmDelete.controller';
import InfoController from './popups/info.controller';

let dashboardModule = angular.module('dashboard', ['ui.bootstrap', 'ui.sortable']);

dashboardModule.controller('dashboardController', DashboardController);
dashboardModule.controller('dashboardConfigurationController', DashboardConfigurationController);
dashboardModule.controller('drillDownController', DrillDownController);
dashboardModule.service('dashboardService', DashboardService);
dashboardModule.component('widgetBox', WidgetComponent);
dashboardModule.component('dashboard', DashboardComponent);
dashboardModule.constant('componentsConstants', ComponentsConstants);
dashboardModule.controller('confirmDeleteController', ConfirmDeleteController);
dashboardModule.controller('infoController', InfoController);

export default dashboardModule;