const ComponentConstants = {
    useApi: true,
    dashboardTemplateUrl: 'components/dashboard/dashboard.html',
    configurationTemplateUrl: 'components/dashboard/dashboardConfiguration.html',
    drillDownChartTemplateUrl: 'components/dashboard/drillDownChart.html',
    drillDownTableTemplateUrl: 'components/dashboard/drillDownTable.html',
    drillDownListTemplateUrl: 'components/dashboard/drillDownList.html',
    confirmDeleteTemplateUrl: 'components/popups/confirmDelete.html',
    infoTemplateUrl: 'components/popups/info.html',
    getDashboardDataApi: 'http://localhost:8090/nessSmart/dashboard/getDashboard?userId=',
    saveDashboardApi: 'http://localhost:8090/nessSmart/dashboard/saveDashboard',
    deleteDashboardApi: 'http://localhost:8090/nessSmart/dashboard/deleteDashboard?dashboardId=',
    editConfigurationApi: 'http://localhost:8090/nessSmart/dashboard/updateUserDetails'
};

export default ComponentConstants;