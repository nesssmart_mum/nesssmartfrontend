class ApiService {
    constructor($http) {
        'ngInject'
        this.http = $http;
    }

    getDummyData(url) {
        return this.http({
            url: url,
            method: 'GET',
            dataType: 'json',
            data: '',
            headers: {
                'Content-Type': 'application/json',
                'JsonStub-User-Key': 'f3748baa-5483-4d92-bc8a-fb77f3c99872',
                'JsonStub-Project-Key': '7f69205d-b3c5-47f9-bad6-d603b579d27a'
            }
        });
    }

    getData(url) {
        return this.http({
            url: url,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    postData(url, data) {
        return this.http({
            url: url,
            method: 'POST',
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    putData(url, data) {
        return this.http({
            url: url,
            method: 'PUT',
            data: data,
            headers: {
                'Content-Type': 'application/json',
                'JsonStub-User-Key': 'f3748baa-5483-4d92-bc8a-fb77f3c99872',
                'JsonStub-Project-Key': '7f69205d-b3c5-47f9-bad6-d603b579d27a'
            }
        });
    }

    deleteData(url) {
        return this.http({
            url: url,
            method: 'DELETE'
                // headers: {
                //     'Content-Type': 'application/json'
                // }
        });
    }
}

export default ApiService;