class UtilService {
    constructor($window) {
        'ngInject'

        this.$window = $window;
    }

    showLoader() {
        angular.element(document).find('#loader').removeClass("no-loader");
    };

    hideLoader() {
        angular.element(document).find('#loader').addClass("no-loader");
    };

    getFromSession(entity) {
        return (this.$window.sessionStorage[entity]) ? angular.fromJson(this.$window.sessionStorage[entity]) : null;
    };

    storeInSession(entity, data) {
        this.$window.sessionStorage[entity] = angular.toJson(data);
    };

    clearSession() {
        this.$window.sessionStorage.clear();
    }
}

export default UtilService;