import angular from 'angular';
import ApiService from './services/api.service';
import UtilService from './services/util.service';

let commonModule = angular.module('nessSmart.common', []);

commonModule.service('apiService', ApiService)
commonModule.service('utilService', UtilService)

export default commonModule;