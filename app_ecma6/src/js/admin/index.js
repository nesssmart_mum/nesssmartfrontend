import angular from 'angular';
import AddQuestionController from './addQuestion.controller';
import SelectQuestionController from './selectQuestion.controller';
import QuestionBankController from './questionBank.controller';
import CreateAssessmentController from './createAssessment.controller';
import ManageAssessmentsController from './manageAssessments.controller';
import AdminService from './admin.service';

let adminModule = angular.module('nessSmart.admin', []);

adminModule.controller('addQuestionController', AddQuestionController);
adminModule.controller('selectQuestionController', SelectQuestionController);
adminModule.controller('questionBankController', QuestionBankController);
adminModule.controller('createAssessmentController', CreateAssessmentController);
adminModule.controller('manageAssessmentsController', ManageAssessmentsController);
adminModule.service('adminService', AdminService)

export default adminModule;