import angular from 'angular';

class QuestionBankController {
    constructor($filter, adminService) {
        'ngInject'

        let questions = [];
        this.questionList = [];

        this.getQuestions = function() {
            adminService.getAllQuestionsFromQuestionBank().then(
                (response) => {
                    questions = response.data
                    this.questionList = questions;
                },
                (error) => {
                    console.log(error);
                }
            );
        };

        // this.getProjectQuestions = function() {
        //     if (this.projectType === '') {
        //         this.questionList = questions;
        //     } else {
        //         this.questionList = $filter('filter')(questions, { projectType: this.projectType });
        //     }
        // };

        this.filterQuestions = function() {
            if (this.questionTag) {
                this.questionList = $filter('filter')(questions, { questionTag: this.questionTag }, true)
            } else {
                this.questionList = questions;
            }
        };
    }
}

export default QuestionBankController;