import SelectQuestionController from './selectQuestion.controller';
import InfoController from '../components/popups/info.controller';

class CreateAssessmentController {
    constructor($scope, $state, $stateParams, $uibModal, adminService, assessmentService, utilService) {
        'ngInject'

        let headingObj = {
            title: 'Heading 1',
            questionList: []
        };

        let levelObj = {
            title: 'Level 1',
            headingList: [angular.copy(headingObj)]
        };

        let activityObj = {
            activityId: '',
            activityDescription: 'Activity 1'
        };

        let sectionObj = {
            title: 'Section 1',
            projectType: '',
            activityList: [angular.copy(activityObj)],
            levelList: [angular.copy(levelObj)]
        };

        this.getQuestions = function() {
            adminService.getAllQuestionsFromQuestionBank().then(
                (response) => {
                    this.questionList = response.data;
                },
                (error) => {
                    console.log(error);
                }
            );
        };

        this.loadAssessment = function() {
            if ($stateParams.assessmentId) {
                this.title = 'Edit Assessment';
                assessmentService.getAssessment($stateParams.assessmentId).then(
                    (response) => {
                        this.assessment = response.data;
                    }
                );
            } else {
                this.title = 'Create Assessment';
                this.assessment = {
                    title: 'Assessment 1',
                    sectionList: [angular.copy(sectionObj)]
                };
            }
            this.getQuestions();
        };

        this.addSection = function() {
            var sectionCount = this.assessment.sectionList.length + 1;
            var newSection = angular.copy(sectionObj);
            newSection.title = 'Section ' + sectionCount;
            this.assessment.sectionList.push(newSection);
        };

        this.removeSection = function(section) {
            var sIndex = this.assessment.sectionList.indexOf(section);
            this.assessment.sectionList.splice(sIndex, 1);
        };

        this.addActivity = function(activityList) {
            debugger;
            var activityCount = activityList.length + 1;
            var newActivity = angular.copy(activityObj);
            newActivity.activityDescription = 'Activity ' + activityCount;
            activityList.push(newActivity);
        };

        this.removeActivity = function(activityList, activity) {
            var aIndex = activityList.indexOf(activity);
            activityList.splice(aIndex, 1);
        };

        this.addLevel = function(levelList) {
            var levelCount = levelList.length + 1;
            var newLevel = angular.copy(levelObj);
            newLevel.title = 'Level ' + levelCount;
            levelList.push(newLevel);
        };

        this.removeLevel = function(levels, level) {
            var lIndex = levels.indexOf(level);
            levels.splice(lIndex, 1);
        };

        this.addHeading = function(headingList) {
            var headingCount = headingList.length + 1;
            var newHeading = angular.copy(headingObj);
            newHeading.title = 'Heading ' + headingCount;
            headingList.push(newHeading);
        };

        this.addQuestions = function(questionList) {
            $scope.questions = this.questionList;
            $uibModal.open({
                templateUrl: 'admin/selectQuestion.html',
                controller: SelectQuestionController,
                controllerAs: 'ctrl',
                size: 'lg',
                scope: $scope
            }).result.then((questions) => {
                angular.forEach(questions, (question) => {
                    questionList.push(question);
                });
            });
        };

        this.removeQuestion = function(questions, question) {
            var qIndex = questions.indexOf(question);
            questions.splice(qIndex, 1);
        };

        this.removeHeading = function(headings, heading) {
            var hIndex = headings.indexOf(heading);
            headings.splice(hIndex, 1);
        };

        this.createAssessment = function(form) {
            utilService.showLoader();
            var emptyList = false,
                minlevels = false;
            this.submitted = true;
            if (!form.$valid) {
                this.showError = true;
                this.errorText = 'All fields are mandatory';
                utilService.hideLoader();
                return;
            }
            angular.forEach(this.assessment.sectionList, (section) => {
                if (section.levelList.length < 5) {
                    minlevels = true;
                }
                angular.forEach(section.levelList, (level) => {
                    angular.forEach(level.headingList, (heading) => {
                        if (heading.questionList.length === 0) {
                            emptyList = true;
                        }
                    });
                });
            });

            if (minlevels) {
                this.showError = true;
                this.errorText = 'Each section should have exactly 5 levels';
                utilService.hideLoader();
                return;
            }

            if (emptyList) {
                this.showError = true;
                this.errorText = 'Please select questions for headings';
                utilService.hideLoader();
                return;
            }

            adminService.createAssessment(this.assessment).then(
                () => {
                    utilService.hideLoader();
                    $scope.message = 'Assessment saved successfully';
                    $uibModal.open({
                        templateUrl: 'components/popups/info.html',
                        controller: InfoController,
                        controllerAs: 'ctrl',
                        size: 'sm',
                        scope: $scope
                    }).result.finally(() => {
                        $state.go('admin.manageAssessments');
                    });
                },
                (error) => {
                    utilService.hideLoader();
                    console.log(error);
                }
            );

        };
    }
}

export default CreateAssessmentController;