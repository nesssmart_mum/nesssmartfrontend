import InfoController from '../components/popups/info.controller';
class AddQuestionController {
    constructor($scope, $state, $uibModal, adminService) {
        'ngInject'

        let question = {
            questionText: '',
            questionType: '',
            projectType: '',
            questionTag: '',
            options: [
                { optionText: '', weight: '', score: '' },
                { optionText: '', weight: '', score: '' }
            ]
        };

        this.questionList = [angular.copy(question)];

        this.addQuestions = function() {
            this.questionList.push(angular.copy(question));
        };

        this.removeQuestion = function(question) {
            var qIndex = this.questionList.indexOf(question);
            this.questionList.splice(qIndex, 1);
        };

        this.addOptions = function(optionList) {
            optionList.push({ optionText: '', weight: '', score: '' });
        };

        this.removeOption = function(optionList, option) {
            var oIndex = optionList.indexOf(option);
            optionList.splice(oIndex, 1);
        };

        this.saveQuestions = function(form) {
            this.submitted = true;
            if (!form.$valid) {
                this.showError = true;
                return;
            }
            angular.forEach(this.questionList, (question) => {
                if (question.questionType === "3") {
                    question.options = [];
                }
            });
            adminService.addQuestionsInQuestionBank(this.questionList).then(() => {
                $scope.message = 'Question(s) added successfully';
                $uibModal.open({
                    templateUrl: 'components/popups/info.html',
                    controller: InfoController,
                    controllerAs: 'ctrl',
                    size: 'sm',
                    scope: $scope
                }).result.finally(() => {
                    $state.go('admin.questionBank');
                });
            }, (error) => {
                console.log(error);
            });
        };
    }
}

export default AddQuestionController;