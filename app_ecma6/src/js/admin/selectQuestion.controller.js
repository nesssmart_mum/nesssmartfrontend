class SelectQuestionController {
    constructor($scope, $filter, $uibModalInstance) {
        'ngInject'

        let questions = angular.copy($scope.questions);
        this.questions = $scope.questions;
        this.selectedQuestions = {};

        this.filterQuestions = function() {
            if (this.questionTag) {
                this.questions = $filter('filter')(questions, { questionTag: this.questionTag }, true);
            } else {
                this.questions = questions;
            }
        };

        this.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

        this.add = function() {
            var selectedQuestions = [];
            angular.forEach(this.selectedQuestions, (value) => {
                if (value && value !== '') {
                    selectedQuestions.push(value);
                }
            });
            if (selectedQuestions.length == 0) {
                this.showError = true;
                return;
            }
            $uibModalInstance.close(selectedQuestions);
        };
    }
}

export default SelectQuestionController;