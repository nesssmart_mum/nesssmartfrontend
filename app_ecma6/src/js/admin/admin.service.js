class AdminService {
    constructor(constants, apiService) {
        'ngInject'
        this.baseUrl = constants.apiUrl + 'assessment/';
        this.apiService = apiService;
    }

    getAllQuestionsFromQuestionBank() {
        var url = this.baseUrl + 'getQuestionBank';
        return this.apiService.getData(url);
    };

    getProjectQuestionsFromQuestionBank(projectType) {
        var url = this.baseUrl + 'getQuestionBank?projectType=' + projectType;
        return this.apiService.getData(url);
    };

    addQuestionsInQuestionBank(data) {
        var url = this.baseUrl + 'saveQuestionBank';
        return this.apiService.postData(url, data);
    };

    createAssessment(data) {
        var url = this.baseUrl + 'saveAssessment';
        return this.apiService.postData(url, data);
    };

    deleteAssessment(assessmentId) {
        var url = this.baseUrl + 'deleteAssessment?assessmentId=' + assessmentId;
        return this.apiService.deleteData(url);
    };
}

export default AdminService;