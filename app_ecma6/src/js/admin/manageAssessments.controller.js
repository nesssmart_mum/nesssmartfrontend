import ConfirmDeleteController from '../components/popups/confirmDelete.controller';
class ManageAssessmentsController {
    constructor($scope, $state, $uibModal, assessmentService, adminService) {
        'ngInject'

        this.getAssessments = function() {
            assessmentService.getAssessmentList().then(
                (response) => {
                    this.assessmentList = response.data;
                }
            );
        };

        this.editAssessment = function(assessmentId) {
            $state.go('admin.editAssessment', { assessmentId: assessmentId });
        };

        this.deleteAssessment = function(assessment) {
            $scope.message = 'Are you sure you want to delete this assessment ?';
            $uibModal.open({
                templateUrl: 'components/popups/confirmDelete.html',
                controller: ConfirmDeleteController,
                controllerAs: 'ctrl',
                size: 'md',
                scope: $scope
            }).result.then(() => {
                adminService.deleteAssessment(assessment.assessmentId).then(() => {
                    var aIndex = this.assessmentList.indexOf(assessment);
                    this.assessmentList.splice(aIndex, 1);
                });
            });
        };
    }
}

export default ManageAssessmentsController;