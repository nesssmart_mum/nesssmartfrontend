class HomeService {
    constructor(constants, apiService) {
        'ngInject'
        this.widgetsUrl = constants.apiUrl + 'dashboard/';
        this.qualityTrendUrl = constants.apiUrl + 'qualityWidget/';
        this.pipelineTrendUrl = constants.apiUrl + 'developmentPipelineTrendWidget/';
        this.releaseStatusUrl = constants.apiUrl + 'releasestatusWidget/';
        this.productionReportingUrl = constants.apiUrl + 'productionReportingWidget/';
        this.apiService = apiService;
    }

    getWidgets() {
        var url = this.widgetsUrl + 'getAllWidgets';
        return this.apiService.getData(url);
    };

    releaseStatusJiraData(resource) {
        var url = this.releaseStatusUrl + 'getJiraReleaseStatus?resourceName=' + resource;
        return this.apiService.getData(url);
    };

    releaseStatusTestsData(project, dev, int, test, prod) {
        var url = this.releaseStatusUrl + 'getFunctionalTestStatus?projectName=' + project + '&dev_projectName=' + dev + '&int_projectName=' + int + '&test_projectName=' + test + '&prod_projectName=' + prod;
        return this.apiService.getData(url);
    };

    releaseStatusMappingData(resource) {
        var url = this.releaseStatusUrl + 'getReleaseManifestMapping?resourceName=' + resource;
        return this.apiService.getData(url);
    };

    pipelineTrendFrequencyData(project, date, env) {
        var url = this.pipelineTrendUrl + 'getProductionDeploymentFrequency?projectName=' + project + '&buildDeployeDate=' + date + '&env=' + env;
        return this.apiService.getData(url);
    };

    pipelineTrendLeadTimeData(project, devEnv, prodEnv) {
        var url = this.pipelineTrendUrl + 'getAverageLeadTimeforProductionChanges?prod_projectName=' + project + '&dev_env=' + devEnv + '&prod_env=' + prodEnv;
        return this.apiService.getData(url);
    };

    pipelineTrendRecoverTimeData(resource) {
        var url = this.pipelineTrendUrl + 'getJiraRecoveryTimeSpent?resourceName=' + resource;
        return this.apiService.getData(url);
    };

    pipelineTrendFailureRateData() {
        var url = this.pipelineTrendUrl + 'getProductionFailureRate';
        return this.apiService.getData(url);
    };

    qualityTrendTestData(resource) {
        var url = this.qualityTrendUrl + 'jenkinsSuccessRatio?resourceName=' + resource;
        return this.apiService.getData(url);
    };

    qualityTrendCodeData(resource) {
        var url = this.qualityTrendUrl + 'jenkinsSonarData?resourceName=' + resource;
        return this.apiService.getData(url);
    };

    qualityTrendAutomationData(projectId) {
        var url = this.qualityTrendUrl + 'testlink?projectId=' + projectId;
        return this.apiService.getData(url);
    };

    productionReportingP1Data(resource) {
        var url = this.productionReportingUrl + 'getJiraIssues?resourceName=' + resource;
        return this.apiService.getData(url);
    };

    productionReportingLogData() {
        var url = this.productionReportingUrl + 'getProductionLogsAnalysis';
        return this.apiService.getData(url);
    };
}

export default HomeService;