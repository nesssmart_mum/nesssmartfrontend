import angular from 'angular';
import HomeController from './home.controller';
import HomeService from './home.service';

let homeModule = angular.module('nessSmart.home', []);

homeModule.controller('homeController', HomeController);
homeModule.service('homeService', HomeService);

export default homeModule;