class HomeController {
    constructor($window, homeService) {
        'ngInject'

        this.widgetsData = {
            releaseStatus: {},
            pipelineTrend: {},
            qualityTrend: {},
            productionReporting: {}
        };

        this.loadWidgets = function() {
            homeService.getWidgets().then((response) => {
                this.widgets = response.data;
                this.loadWidgetData();
            });
        };

        this.loadWidgetData = function() {
            // Release Status Widget Data
            homeService.releaseStatusJiraData('NessSmartBackEnd').then((result) => {
                this.widgetsData.releaseStatus.jiraData = result.data;
            });

            homeService.releaseStatusTestsData('NessSmart', 'DEV', 'INT', 'TEST', 'PROD').then((result) => {
                this.widgetsData.releaseStatus.testsData = result.data;
            });

            homeService.releaseStatusMappingData('NessSmartBackEnd').then((result) => {
                this.widgetsData.releaseStatus.mappingData = result.data;
            });

            // Pipeline Trend Widget Data            

            homeService.pipelineTrendFrequencyData('NessSmart', '07%2F06%2F2017', 'PROD').then((result) => {
                this.widgetsData.pipelineTrend.frequencyData = result.data;
            });

            homeService.pipelineTrendLeadTimeData('NessSmart', 'DEV', 'PROD').then((result) => {
                this.widgetsData.pipelineTrend.leadTimeData = result.data;
            });

            homeService.pipelineTrendRecoverTimeData('NessSmartBackEnd').then((result) => {
                this.widgetsData.pipelineTrend.recoverTimeData = result.data;
            });

            homeService.pipelineTrendFailureRateData().then((result) => {
                this.widgetsData.pipelineTrend.failureRateData = result.data;
            });

            //Quality Trend Widget Data
            homeService.qualityTrendTestData('NessSmartBackEnd').then((result) => {
                this.widgetsData.qualityTrend.testData = result.data;
            });

            homeService.qualityTrendCodeData('NessSmartBackEnd').then((result) => {
                this.widgetsData.qualityTrend.codeData = result.data;
            });

            homeService.qualityTrendAutomationData(16).then((result) => {
                this.widgetsData.qualityTrend.automationData = result.data;
            });

            // Production Reporting Widget Data
            homeService.productionReportingP1Data('NessSmartBackEnd').then((result) => {
                this.widgetsData.productionReporting.p1Data = result.data;
            });

            homeService.productionReportingLogData().then((result) => {
                this.widgetsData.productionReporting.logData = result.data;
            });

        };
    }
}

export default HomeController;