import angular from 'angular';
import LoginController from './login.controller';
import LoginService from './login.service';

let loginModule = angular.module('nessSmart.login', []);

loginModule.controller('loginController', LoginController);
loginModule.service('loginService', LoginService);

export default loginModule;