class LoginService {
    constructor($filter) {
        'ngInject'
        this.$filter = $filter;

        this.users = [{
                userId: '5975d43f0a91020b9c734e0a',
                fullName: 'Vasim Mapari',
                email: 'vasim.mapari@ness.com',
                password: 'welcome123',
                role: '',
                dashboardConfig: {
                    columnCount: '2',
                    theme: 'light'
                }
            },
            {
                userId: '5979ebbd0a91021cccc5ef99',
                fullName: 'Amit Gupta',
                email: 'amit.gupta@ness.com',
                password: 'welcome123',
                role: '',
                dashboardConfig: {
                    columnCount: '2',
                    theme: 'light'
                }
            }
        ];
    }

    validateUser(email, password) {
        var users = this.$filter('filter')(this.users, { email: email, password: password }, true);
        return (users.length > 0) ? users[0] : null;
    };
}

export default LoginService