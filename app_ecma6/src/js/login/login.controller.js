class LoginController {
    constructor($rootScope, $state, utilService, loginService) {
        'ngInject'

        utilService.clearSession();

        this.validateUser = function(form) {
            this.submitted = true;
            if (!form.$valid) return;
            var user = loginService.validateUser(this.email, this.password);
            if (user) {
                $rootScope.currentUser = user;
                utilService.storeInSession('currentUser', user);
                $state.go('home');
            } else {
                this.showError = true;
            }
        }
    }
}

export default LoginController