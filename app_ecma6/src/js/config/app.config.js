function AppConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    'ngInject';

    $locationProvider.hashPrefix('');
    $stateProvider
        .state("login", {
            url: "/",
            templateUrl: 'login/login.html',
            controller: 'loginController',
            controllerAs: 'ctrl',
            resolve: {
                style: function() {
                    var element = document.getElementById('navigationLinks');
                    angular.element(element).css('display', 'none');
                }
            }
        })
        .state("home", {
            url: "/home",
            templateUrl: 'home/home.html',
            controller: 'homeController',
            controllerAs: 'ctrl',
            resolve: {
                style: function() {
                    var element = document.getElementById('navigationLinks');
                    angular.element(element).css('display', '');
                }
            }
        })
        .state("assessment", {
            abstract: true,
            template: '<div ui-view></div>'
        })
        .state("assessment.takeAssessment", {
            url: "/assessment/takeAssessment",
            templateUrl: 'assessment/takeAssessment.html',
            controller: 'takeAssessmentController',
            controllerAs: 'ctrl'
        })
        .state("assessment.editFeedback", {
            url: "/assessment/editFeedback/:feedbackId",
            templateUrl: 'assessment/takeAssessment.html',
            controller: 'takeAssessmentController',
            controllerAs: 'ctrl'
        })
        .state("assessment.manageFeedback", {
            url: "/assessment/manageFeedback",
            templateUrl: 'assessment/manageFeedback.html',
            controller: 'manageFeedbackController',
            controllerAs: 'ctrl'
        })
        .state("assessment.report", {
            url: "/assessment/assessmentReport/:responseId",
            templateUrl: 'assessment/assessmentReport.html',
            controller: 'assessmentReportController',
            controllerAs: 'ctrl'
        })
        .state("admin", {
            abstract: true,
            template: '<div ui-view></div>'
        })
        .state("admin.questionBank", {
            url: "/admin/questionBank",
            templateUrl: 'admin/questionBank.html',
            controller: 'questionBankController',
            controllerAs: 'ctrl'
        })
        .state("admin.addQuestion", {
            url: "/admin/addQuestion",
            templateUrl: 'admin/addQuestion.html',
            controller: 'addQuestionController',
            controllerAs: 'ctrl'
        })
        .state("admin.createAssessment", {
            url: "/admin/createAssessment",
            templateUrl: 'admin/createAssessment.html',
            controller: 'createAssessmentController',
            controllerAs: 'ctrl'
        })
        .state("admin.editAssessment", {
            url: "/admin/createAssessment/:assessmentId",
            templateUrl: 'admin/createAssessment.html',
            controller: 'createAssessmentController',
            controllerAs: 'ctrl'
        })
        .state("admin.manageAssessments", {
            url: "/admin/manageAssessments",
            templateUrl: 'admin/manageAssessments.html',
            controller: 'manageAssessmentsController',
            controllerAs: 'ctrl'
        });

    $urlRouterProvider.otherwise('/');
}

export default AppConfig;