function AppRun($rootScope, $state, $timeout, utilService) {
    'ngInject';

    $rootScope.$on('$stateChangeStart', (event, toState) => {
        if (!$rootScope.currentUser) {
            $rootScope.currentUser = utilService.getFromSession('currentUser');
            if (!$rootScope.currentUser && toState.name !== 'login') {
                $timeout(() => { $state.go('login') }, 100);
            }
        }
        utilService.showLoader();
    });

    $rootScope.$on('$stateChangeSuccess', (event, toState) => {
        utilService.hideLoader();
    });
}

export default AppRun;