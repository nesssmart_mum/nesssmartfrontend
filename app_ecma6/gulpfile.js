var gulp = require('gulp');
var notify = require('gulp-notify');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var babelify = require('babelify');
var ngAnnotate = require('browserify-ngannotate');
var browserSync = require('browser-sync').create();
var rename = require('gulp-rename');
var templateCache = require('gulp-angular-templatecache');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify');
var merge = require('merge-stream');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

// Where our files are located
var jsFiles = "src/js/**/*.js";
var cssFiles = "src/styles/*.css";
var scssFiles = "src/styles/*.scss";
var viewFiles = "src/js/**/*.html";

var interceptErrors = function(error) {
    var args = Array.prototype.slice.call(arguments);

    // Send error to notification center with gulp-notify
    notify.onError({
        title: 'Compile Error',
        message: '<%= error.message %>'
    }).apply(this, args);

    // Keep gulp from hanging on this task
    this.emit('end');
};


gulp.task('browserify', ['views'], function() {
    return browserify('./src/js/app.js')
        .transform(babelify, { presets: ["es2015"] })
        .transform(ngAnnotate)
        .bundle()
        .on('error', interceptErrors)
        .pipe(source('main.js'))
        .pipe(gulp.dest('./build/'));
});

gulp.task('html', function() {
    return gulp.src("src/index.html")
        .on('error', interceptErrors)
        .pipe(gulp.dest('./build/'));
});

gulp.task('css', function() {

    var scssStream = gulp.src("src/styles/*.scss")
        .pipe(sass())
        .pipe(minify())
        .pipe(concat('scss-files.scss'));

    var cssStream = gulp.src("src/styles/*.css")
        .pipe(concat('css-files.css'));

    var mergedStream = merge(scssStream, cssStream)
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('./build/'));

    return mergedStream;
});

gulp.task('fonts', function() {
    gulp.src('src/fonts/*.*')
        .pipe(gulp.dest('./build/fonts'));
});

gulp.task('views', function() {
    return gulp.src(viewFiles)
        .pipe(templateCache({
            standalone: true
        }))
        .on('error', interceptErrors)
        .pipe(rename("app.templates.js"))
        .pipe(gulp.dest('./src/js/config/'));
});

// This task is used for building production ready
// minified JS/CSS files into the dist/ folder
// gulp.task('build', ['html', 'browserify', 'scss', 'fonts'], function() {
//     var html = gulp.src("build/index.html")
//         .pipe(gulp.dest('./dist/'));

//     var js = gulp.src("build/main.js")
//         .pipe(uglify())
//         .pipe(gulp.dest('./dist/'));

//     var scss = gulp.src("build/styles.scss")
//         .pipe(uglify())
//         .pipe(gulp.dest('./dist/'));

//     var fonts = gulp.src("build/fonts")
//         .pipe(gulp.dest('./dist/'));

//     return merge(html, js, scss, fonts);
// });

gulp.task('default', ['html', 'browserify', 'css', 'fonts'], function() {

    browserSync.init(['./build/**/**.**'], {
        server: "./build",
        port: 8089,
        notify: false,
        ui: {
            port: 8089
        },
        ghostMode: false
    });

    gulp.watch("src/index.html", ['html']);
    gulp.watch(viewFiles, ['views']);
    gulp.watch(jsFiles, ['browserify']);
    gulp.watch(cssFiles, ['css']);
    gulp.watch(scssFiles, ['css']);
});